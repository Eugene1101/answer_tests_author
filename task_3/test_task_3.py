import pytest
import time

from task_3.author import long_heavy




@pytest.mark.parametrize('num', [(3,), (4,)])
def test_time_run_task_3(num):
    time_start = time.time()
    result = long_heavy(num)
    exec_time_1 = round(time.time() - time_start, 1)
    time_start = time.time()
    result = long_heavy(num)
    exec_time_2 = round(time.time() - time_start, 1)
    assert exec_time_1 > exec_time_2, 'Время повторного выполнения функции с одним значением больше или такое же как первичное.'
