import pytest

from task_4.author import make_divider_of


@pytest.mark.parametrize('divider, divisible, result', [(2, 10, 5), (5, 20, 4)])
def test_make_divider(divider, divisible, result):
    div = make_divider_of(divider)
    assert div(divisible) == result,'Результат деления неверный'
