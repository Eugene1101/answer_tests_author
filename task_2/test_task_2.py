import sys
import pytest
from task_2.author import Contact


@pytest.mark.parametrize('name, phone, birthday, address', [('Михаил Булгаков',
                                                             '2-03-27', '15.05.1891',
                                                             'Россия, Москва, Большая Пироговская,'
                                                            'дом 35б, кв. 6')])

def test_output_task_2(name, phone, birthday, address):
    with open('text.txt', 'w+', encoding='utf-8') as file:
        obj = Contact(name, phone, birthday, address)
        sys.stdout = file
        obj.show_contact()
        result = f'{name} — адрес: {address}, телефон: {phone}, день рождения: {birthday}'
        file.seek(0, 0)
        result_show_contact = file.read().rstrip('\n')
    assert result_show_contact == result, 'Ошибка в выводе метода класса Contact'



